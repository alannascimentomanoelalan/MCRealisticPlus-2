package net.islandearth.mcrealistic.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Subcommand;
import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.gui.ItemGUI;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandAlias("mcrealistic|mcr")
public class MCRealisticCommand extends BaseCommand {

    private final MCRealistic plugin;

    public MCRealisticCommand(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @Default
    public void onDefault(CommandHelp help) {
        help.showHelp();
    }

    @Subcommand("reload")
    @CommandPermission("mcr.reload")
    public void onReload(CommandSender sender) {
        sender.sendMessage(ChatColor.GREEN + "Reloading...");
        plugin.reloadConfig();
        plugin.saveConfig();
        sender.sendMessage(ChatColor.GREEN + "Done!");
    }

    @Subcommand("items")
    @CommandPermission("mcr.items")
    public void onItems(Player player) {
        new ItemGUI(plugin, player).open();
    }
}
