package net.islandearth.mcrealistic.tasks;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

public class MiscTask implements Runnable {

    private final MCRealistic plugin;
    private final List<World> worlds;

    public MiscTask(MCRealistic plugin) {
        this.plugin = plugin;
        this.worlds = plugin.getWorlds();
    }

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (worlds.contains(player.getWorld())) {
                if (player.getGameMode() == GameMode.SURVIVAL && !player.isDead()) {
                    MCRealisticPlayer mcRealisticPlayer = plugin.getCache().getPlayer(player);
                    if (mcRealisticPlayer == null) return;
                    int currentFatigue = mcRealisticPlayer.getFatigue();

                    if (player.getHealth() < 6.0 && getConfig().getBoolean("Server.Player.DisplayHurtMessage")) {
                        player.setSprinting(false);
                        player.setSneaking(true);
                        Translations.HURT.send(player);
                    }

                    if (getConfig().getBoolean("Server.Player.Allow Fatigue")) {
                        if (currentFatigue >= getConfig().getInt("Server.Player.Max Fatigue")) {
                            Translations.VERY_TIRED.send(player);
                            player.damage(3.0);
                            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1200, 1));
                            player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 80, 2));
                        }

                        if (currentFatigue >= getConfig().getInt("Server.Player.Fatigue Tired Range Min")
                                && currentFatigue <= getConfig().getInt("Server.Player.Fatigue Tired Range Max")) {
                            Translations.TIRED.send(player);
                            player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 40, 1));
                        }
                    }

                    if (!getConfig().getBoolean("Server.Weather.WeatherAffectsPlayer")) continue;

                    if (player.getWorld().hasStorm()) {
                        if (player.getInventory().getBoots() != null
                                && player.getInventory().getChestplate() != null) {
                            if (!mcRealisticPlayer.hasBrokenBones()) {
                                mcRealisticPlayer.setWarm(true);
                            }
                        } else if (player.getInventory().getBoots() == null
                                && player.getInventory().getChestplate() == null
                                && !mcRealisticPlayer.isNearWarmthSource()) {
                            if (player.getWorld().getHighestBlockYAt(player.getLocation()) < player.getLocation().getY()) {
                                Translations.COLD.send(player);
                                mcRealisticPlayer.setFatigue(mcRealisticPlayer.getFatigue() + 10);
                                mcRealisticPlayer.setWarm(false);
                                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 500, 0));
                                player.damage(3.0);
                            } else {
                                mcRealisticPlayer.setWarm(true);
                            }
                        }
                    }

                    if (!player.getWorld().hasStorm()
                            && !mcRealisticPlayer.hasBrokenBones()) {
                        mcRealisticPlayer.setWarm(true);
                    }

                    if (!mcRealisticPlayer.isNearWarmthSource()
                            || !player.getWorld().hasStorm() || mcRealisticPlayer.hasBrokenBones())
                        continue;

                    Translations.COSY.send(player);
                    mcRealisticPlayer.setFatigue(mcRealisticPlayer.getFatigue() - 1);
                }
            }
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }

}
