package net.islandearth.mcrealistic;

import co.aikar.commands.PaperCommandManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.islandearth.languagy.api.language.Language;
import net.islandearth.languagy.api.language.LanguagyImplementation;
import net.islandearth.languagy.api.language.LanguagyPluginHook;
import net.islandearth.languagy.api.language.Translator;
import net.islandearth.mcrealistic.api.MCRealisticAPI;
import net.islandearth.mcrealistic.commands.Fatigue;
import net.islandearth.mcrealistic.commands.MCRealisticCommand;
import net.islandearth.mcrealistic.commands.Thirst;
import net.islandearth.mcrealistic.listeners.BlockListener;
import net.islandearth.mcrealistic.listeners.ConnectionListener;
import net.islandearth.mcrealistic.listeners.ConsumeListener;
import net.islandearth.mcrealistic.listeners.EntityListener;
import net.islandearth.mcrealistic.listeners.FoodChangeListener;
import net.islandearth.mcrealistic.listeners.InteractListener;
import net.islandearth.mcrealistic.listeners.InventoryListener;
import net.islandearth.mcrealistic.listeners.MoveListener;
import net.islandearth.mcrealistic.listeners.ProjectileListener;
import net.islandearth.mcrealistic.listeners.RespawnListener;
import net.islandearth.mcrealistic.managers.MCRealisticManagers;
import net.islandearth.mcrealistic.placeholders.MCRealisticPlaceholders;
import net.islandearth.mcrealistic.player.MCRealisticCache;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.player.PlayerListener;
import net.islandearth.mcrealistic.tasks.CooldownFixTask;
import net.islandearth.mcrealistic.tasks.CosyTask;
import net.islandearth.mcrealistic.tasks.DiseaseTask;
import net.islandearth.mcrealistic.tasks.MiscTask;
import net.islandearth.mcrealistic.tasks.StaminaTask;
import net.islandearth.mcrealistic.tasks.ThirstTask;
import net.islandearth.mcrealistic.tasks.TorchTask;
import net.islandearth.mcrealistic.tasks.WindTask;
import net.islandearth.mcrealistic.translation.Translations;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Tag;
import org.bukkit.World;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class MCRealistic extends JavaPlugin implements MCRealisticAPI, LanguagyPluginHook {

    @LanguagyImplementation(Language.ENGLISH)
    private Translator translator;
    private List<World> worlds = new ArrayList<>();
    private List<UUID> colds = new ArrayList<>();
    private List<UUID> diseases = new ArrayList<>();
    private List<UUID> burning = new ArrayList<>();
    private MCRealisticManagers managers;
    private MCRealisticCache cache;

    private static MCRealistic plugin;

    @Override
    public Translator getTranslator() {
        return translator;
    }

    public List<World> getWorlds() {
        return worlds;
    }

    public List<UUID> getColds() {
        return colds;
    }

    public List<UUID> getDiseases() {
        return diseases;
    }

    public List<UUID> getBurning() {
        return burning;
    }

    public MCRealisticManagers getManagers() {
        return managers;
    }

    public MCRealisticCache getCache() {
        return cache;
    }

    @Override
    public void onEnable() {
        MCRealistic.plugin = this;

        createFiles();
        hookPlugins();
        this.managers = new MCRealisticManagers(this);
        this.cache = new MCRealisticCache();
        registerListeners();
        registerCommands();
        registerRecipes();
        startTasks();
        this.hook(this);

        new Metrics(this, 2300);

        for (String s : getConfig().getStringList("Worlds")) {
            World w = Bukkit.getWorld(s);
            if (w != null) worlds.add(w);
        }

        Bukkit.getOnlinePlayers().forEach(player -> {
            File file = new File(this.getDataFolder() + "/players/" + player.getUniqueId() + ".json");
            if (!file.exists()) {
                cache.addPlayer(player);
            } else {
                try {
                    Reader reader = new FileReader(file);
                    MCRealisticPlayer mcRealisticPlayer = getGson().fromJson(reader, MCRealisticPlayer.class);
                    cache.addPlayer(mcRealisticPlayer);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onDisable() {
        this.getLogger().info("Disabling...");
        Bukkit.getOnlinePlayers().forEach(player -> {
            MCRealisticPlayer mcRealisticPlayer = cache.getPlayer(player);
            if (mcRealisticPlayer != null) {
                try {
                    mcRealisticPlayer.save(this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        // Do this to solve some reload issues.
        this.translator = null;
        this.worlds = new ArrayList<>();
        this.colds = new ArrayList<>();
        this.diseases = new ArrayList<>();
        this.burning = new ArrayList<>();
    }

    private void createFiles() {
        File folder = new File(plugin.getDataFolder() + "/players/");
        if (!folder.exists()) folder.mkdirs();
        createConfig();
        Translations.generateLang(this);
    }

    private void createConfig() {
        //TODO makes classes to manage config
        File file = new File("plugins/MCRealistic-2/config.yml");
        if (!file.exists()) {

            List<String> worlds = new ArrayList<>();
            worlds.add("world");

            getConfig().options().copyDefaults(true);

            String header;
            String eol = System.getProperty("line.separator");
            header = "MCRealistic-2 Properties:" + eol;
            header += eol;
            header += "Worlds" + eol;
            header += "  Here you define which worlds you enable." + eol;
            header += eol;
            header += "WeatherAffectsPlayer" + eol;
            header += "  This option defines whether the player should be affected by the weather. Default true." + eol;
            header += eol;
            header += "Thirst" + eol;
            header += "  This option defines whether thirst is enabled. Default true." + eol;
            header += eol;
            header += "DisplayHungerMessage" + eol;
            header += "  Whether the hunger message should be shown. Default true." + eol;
            header += eol;
            header += "Cosy" + eol;
            header += "  DisplayMessage" + eol;
            header += "  Whether the cozy message should be shown. Default true." + eol;
            header += eol;
            header += "  CallInterval" + eol;
            header += "    Must be greater than 0" + eol;
            header += "    A value of 1 = 30 seconds, 2 = 60 seconds, ect..." + eol;
            header += eol;
            header += "  FoodLevelForRegeneration" + eol;
            header += "    Minimum food level to regenerate health. 0 - 20" + eol;
            header += eol;
            header += "  FoodUseToRecoverHealth" + eol;
            header += "    Food consumed in recovering heart 0 - 20" + eol;
            header += eol;
            header += "  TorchRadius" + eol;
            header += "    Distance from torch to feel \"Cosy\"" + eol;
            header += eol;
            header += "  TorchDuration" + eol;
            header += "    1 = 1/2 a heart recovery" + eol;
            header += eol;
            header += "  FurnaceRadius" + eol;
            header += "    Distance from furnace to feel \"Cosy\"" + eol;
            header += eol;
            header += "  FurnaceDuration" + eol;
            header += "    2 = 1 heart recovery" + eol;
            header += eol;
            header += "  FurnaceMustBeLit" + eol;
            header += "    The furnace has to be lit to feel \"Cosy\"" + eol;
            header += eol;
            header += "  InclementWeatherHeatReduction" + eol;
            header += "    % of distance to heat to feel \"Cosy\" in rain/snow. 0.0 - 1.0" + eol;
            header += "    0.3 = 30% of TorchRadius/FurnaceRadius" + eol;

            header += eol;
            header += "DisplayHurtMessage" + eol;
            header += "  Whether the hurt message would be shown. Default true." + eol;
            header += eol;
            header += "Weight" + eol;
            header += "  This option defines whether the player should be affected by weight." + eol;
            header += eol;
            header += "Realistic_Building" + eol;
            header += "  This option defines whether blocks will fall." + eol;
            header += eol;
            header += "Messages.Type" + eol;
            header += "  MESSAGE, ACTIONBAR, TITLE" + eol;
            header += eol;
            getConfig().options().header(header);

            getConfig().addDefault("settings.integration.name", getIntegration());
            getConfig().addDefault("Worlds", worlds);
            getConfig().addDefault("Server.Weather.WeatherAffectsPlayer", true);
            getConfig().addDefault("Server.Player.Thirst", true);
            getConfig().addDefault("Server.Player.Wind", true);
            getConfig().addDefault("Server.World.Falling_Trees", true);
            getConfig().addDefault("Server.Stamina.Enabled", true);
            getConfig().addDefault("Server.Player.DisplayHungerMessage", true);
            getConfig().addDefault("Server.Player.Cosy.DisplayMessage", true);
            getConfig().addDefault("Server.Player.Cosy.CallInterval", 1);
            getConfig().addDefault("Server.Player.Cosy.FoodLevelForRegeneration", 19);
            getConfig().addDefault("Server.Player.Cosy.FoodUseToRecoverHealth", 3);
            getConfig().addDefault("Server.Player.Cosy.TorchRadius", 1.75);
            getConfig().addDefault("Server.Player.Cosy.TorchDuration", 1);
            getConfig().addDefault("Server.Player.Cosy.FurnaceRadius", 4);
            getConfig().addDefault("Server.Player.Cosy.FurnaceDuration", 2);
            getConfig().addDefault("Server.Player.Cosy.FurnaceMustBeLit", true);
            getConfig().addDefault("Server.Player.Cosy.InclementWeatherHeatReduction", 0.5f);
            getConfig().addDefault("Server.Player.DisplayHurtMessage", true);
            getConfig().addDefault("Server.Player.Weight", true);
            getConfig().addDefault("Server.Player.Broken_Bones.Enabled", true);
            getConfig().addDefault("Server.Building.Realistic_Building", true);
            List<String> ignoredBlocks = new ArrayList<>();
            Collections.addAll(ignoredBlocks, "TORCH", "WALL_TORCH", "REDSTONE_WALL_TORCH", "REDSTONE_TORCH");
            Tag.SIGNS.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.STANDING_SIGNS.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.WALL_SIGNS.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.BUTTONS.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.FENCES.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.FENCE_GATES.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.SLABS.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.LOGS.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.CLIMBABLE.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            getConfig().addDefault("Server.Building.Ignored_Blocks", ignoredBlocks);
            getConfig().addDefault("Server.Player.Trail.Grass_Blocks", Collections.singletonList(
                    "DIRT"));
            getConfig().addDefault("Server.Player.Trail.Sand_Blocks", Collections.singletonList(
                    "SANDSTONE"));
            getConfig().addDefault("Server.Player.Trail.Dirt_Blocks", Collections.singletonList(
                    "GRASS_PATH"));
            getConfig().addDefault("Server.Player.Trail.Path_Blocks", Collections.singletonList(
                    "COBBLESTONE"));
            getConfig().addDefault("Server.Player.Trail.Enabled", true);
            getConfig().addDefault("Server.Player.Allow Fatigue", true);
            getConfig().addDefault("Server.Player.Max Fatigue", 240);
            getConfig().addDefault("Server.Player.Max Thirst", 200);
            getConfig().addDefault("Server.Player.Fatigue Tired Range Min", 150);
            getConfig().addDefault("Server.Player.Fatigue Tired Range Max", 200);
            getConfig().addDefault("Server.Player.Allow Chop Down Trees With Hands", false);
            getConfig().addDefault("Server.Player.Trees have random number of drops", true);
            getConfig().addDefault("Server.Player.Allow /fatigue", true);
            getConfig().addDefault("Server.Player.Allow /thirst", true);
            getConfig().addDefault("Server.Player.Spawn with items", true);
            getConfig().addDefault("Server.Player.Allow Enchanted Arrow", true);
            getConfig().addDefault("Server.Player.Torch_Burn", true);
            getConfig().addDefault("Server.Player.Raw_Food_Illness", true);
            getConfig().addDefault("Server.Messages.Type", "CHAT");
            getConfig().addDefault("Server.Messages.Respawn", true);
            getConfig().addDefault("Server.Player.Thirst.Interval", 6000);
            getConfig().addDefault("Server.Player.Thirst.Enabled", true);
            getConfig().addDefault("Server.Player.Thirst.Require_Purify", true);
            getConfig().addDefault("Server.Player.Immune_System.Interval", 6000);
            getConfig().addDefault("Server.Player.Immune_System.Enabled", true);
            getConfig().addDefault("Server.Player.Immune_System.Req_Players", 2);
            getConfig().addDefault("Server.GameMode.Type", "NORMAL");
            getConfig().addDefault("Server.Player.Weight.Netherite", 3);
            getConfig().addDefault("Server.Player.Weight.Diamond", 3);
            getConfig().addDefault("Server.Player.Weight.Leather", 0);
            getConfig().addDefault("Server.Player.Weight.Chainmail", 1);
            getConfig().addDefault("Server.Player.Weight.Iron", 2);
            getConfig().addDefault("Server.Player.Weight.Gold", 1);
            saveConfig();
        }
    }

    private void hookPlugins() {
        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            this.getLogger().info("PlaceholderAPI found!");
            new MCRealisticPlaceholders(this).register();
        } else {
            this.getLogger().info("PlaceholderAPI not found!");
        }
    }

    private void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new InteractListener(this), this);
        pm.registerEvents(new ConnectionListener(this), this);
        pm.registerEvents(new RespawnListener(this), this);
        pm.registerEvents(new ProjectileListener(this), this);
        pm.registerEvents(new BlockListener(this), this);
        pm.registerEvents(new MoveListener(this), this);
        pm.registerEvents(new ConsumeListener(this), this);
        pm.registerEvents(new InventoryListener(this), this);
        pm.registerEvents(new FoodChangeListener(this), this);
        pm.registerEvents(new EntityListener(this), this);
        pm.registerEvents(new PlayerListener(this), this);
    }

    private void registerCommands() {
        PaperCommandManager manager = new PaperCommandManager(this);
        manager.enableUnstableAPI("help");
        manager.registerCommand(new Fatigue(this));
        manager.registerCommand(new Thirst(this));
        manager.registerCommand(new MCRealisticCommand(this));
    }

    private void registerRecipes() {
        //TODO only enable certain ones depending on settings
        ItemStack medicine = new ItemStack(Material.POTION, 2);
        ItemMeta medicinemeta = medicine.getItemMeta();
        medicinemeta.setDisplayName(ChatColor.GREEN + "Medicine");
        medicinemeta.setLore(Collections.singletonList(ChatColor.WHITE + "Drink to help fight your cold/disease!"));
        medicine.setItemMeta(medicinemeta);

        ShapelessRecipe medicinecraft = new ShapelessRecipe(new NamespacedKey(this, getDescription().getName() + "-medicine"), medicine);
        medicinecraft.addIngredient(Material.GLASS_BOTTLE);
        medicinecraft.addIngredient(Material.APPLE);
        medicinecraft.addIngredient(Material.SPIDER_EYE);
        Bukkit.getServer().addRecipe(medicinecraft);

        ItemStack bandage = new ItemStack(Material.PAPER);
        ItemMeta bm = bandage.getItemMeta();
        bm.setDisplayName(ChatColor.DARK_AQUA + "Bandage");
        bandage.setItemMeta(bm);

        ShapelessRecipe BandageRecipe = new ShapelessRecipe(new NamespacedKey(this, getDescription().getName() + "-bandage"), bandage);
        BandageRecipe.addIngredient(Material.BONE_MEAL);
        BandageRecipe.addIngredient(Material.PAPER);
        Bukkit.getServer().addRecipe(BandageRecipe);

        ItemStack hatchet = new ItemStack(Material.WOODEN_AXE);
        ItemMeta hm = hatchet.getItemMeta();
        hm.setDisplayName(ChatColor.WHITE + "Hatchet");
        hatchet.setItemMeta(hm);

        ShapelessRecipe axe = new ShapelessRecipe(new NamespacedKey(this, getDescription().getName() + "-hatchet"), hatchet);
        axe.addIngredient(Material.STICK);
        axe.addIngredient(Material.FLINT);
        axe.addIngredient(Material.FLINT);
        Bukkit.getServer().addRecipe(axe);

        if (getConfig().getBoolean("Server.Player.Thirst.Require_Purify")) {
            ItemStack water = new ItemStack(Material.POTION);
            PotionMeta wm = (PotionMeta) water.getItemMeta();
            wm.setBasePotionData(new PotionData(PotionType.WATER));
            wm.setLore(Collections.singletonList(ChatColor.GRAY + "Purified"));
            water.setItemMeta(wm);

            FurnaceRecipe purified = new FurnaceRecipe(new NamespacedKey(this, getDescription().getName() + "-purified"), water, Material.POTION, 1, 150);
            Bukkit.getServer().addRecipe(purified);
        }
    }

    private void startTasks() {
        if (getConfig().getBoolean("Server.Stamina.Enabled")) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new StaminaTask(this), 0L, 60L);
        }

        if (getConfig().getBoolean("Server.Player.Torch_Burn")) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new TorchTask(this), 0L, 20L);
        }

        if (getConfig().getBoolean("Server.Player.Wind")) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new WindTask(this), 0L, 100L);
        }

        if (getConfig().getBoolean("Server.Player.Allow Fatigue")) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new CosyTask(this), 0L, 600L);
        }

        if (getConfig().getBoolean("Server.Player.Thirst.Enabled")) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new ThirstTask(this), 0L, getConfig().getInt("Server.Player.Thirst.Interval"));
        }

        if (getConfig().getBoolean("Server.Player.Immune_System.Enabled")) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new DiseaseTask(this), 0L, getConfig().getInt("Server.Player.Immune_System.Interval"));
        }

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new MiscTask(this), 0L, 400L);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new CooldownFixTask(this), 0L, 60L);
    }

    public Gson getGson() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .serializeNulls().create();
    }

    @NotNull
    private String getIntegration() {
        if (Bukkit.getPluginManager().getPlugin("WorldGuard") != null) {
            return "WorldGuard";
        }
        return "Default";
    }

    @Override
    public void onLanguagyHook() {
        translator.setDisplay(Material.WOODEN_AXE);
    }

    public static MCRealisticAPI getAPI() {
        return plugin;
    }
}
