package net.islandearth.mcrealistic.gui;

import com.github.stefvanschie.inventoryframework.Gui;
import com.github.stefvanschie.inventoryframework.GuiItem;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.translation.Translations;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.Collections;

public class ItemGUI extends MCRealisticGUI {

    private final Gui gui;

    public ItemGUI(MCRealistic plugin, Player player) {
        super(plugin, player);
        ItemStack medicine = new ItemStack(Material.POTION, 2);
        ItemMeta medicinemeta = medicine.getItemMeta();
        medicinemeta.setDisplayName(ChatColor.GREEN + "Medicine");
        medicinemeta.setLore(Arrays.asList(ChatColor.WHITE + "Drink to help fight your cold/disease!"));
        medicine.setItemMeta(medicinemeta);

        ItemStack chocolatemilk = new ItemStack(Material.MILK_BUCKET);
        ItemMeta chocolatemilkmeta = chocolatemilk.getItemMeta();
        chocolatemilkmeta.setDisplayName(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Chocolate Milk");
        chocolatemilkmeta.setLore(Arrays.asList(ChatColor.WHITE + "Drink to gain Speed II."));
        chocolatemilk.setItemMeta(chocolatemilkmeta);

        ItemStack bandage = new ItemStack(Material.PAPER);
        ItemMeta bm = bandage.getItemMeta();
        bm.setDisplayName(ChatColor.DARK_AQUA + "Bandage");
        bandage.setItemMeta(bm);

        ItemStack stick = new ItemStack(Material.STICK);
        ItemMeta sm = stick.getItemMeta();
        sm.setDisplayName(ChatColor.WHITE + "Digging Stick");
        sm.setLore(Collections.singletonList(ChatColor.WHITE + "Use this to break gravel and make an axe."));
        stick.setItemMeta(sm);

        this.gui = new Gui(plugin, 1, Translations.ITEMS.get(player));
        StaticPane items = new StaticPane(0, 0, 9, 1);
        int current = 0;
        for (ItemStack itemStack : Arrays.asList(medicine, chocolatemilk, bandage, stick)) {
            items.addItem(new GuiItem(itemStack), current, 0);
            current++;
        }
        gui.addPane(items);
    }

    @Override
    public void open() {
        gui.show(getPlayer());
    }
}
