package net.islandearth.mcrealistic.gui;

import net.islandearth.mcrealistic.MCRealistic;
import org.bukkit.entity.Player;

public abstract class MCRealisticGUI {

    private final MCRealistic plugin;
    private final Player player;

    public MCRealisticGUI(MCRealistic plugin, Player player) {
        this.plugin = plugin;
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public abstract void open();
}
