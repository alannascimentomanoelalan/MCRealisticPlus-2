package net.islandearth.mcrealistic.player;

import net.islandearth.mcrealistic.MCRealistic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class PlayerListener implements Listener {

    private final MCRealistic plugin;

    public PlayerListener(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        File file = new File(plugin.getDataFolder() + "/players/" + player.getUniqueId() + ".json");
        if (!file.exists()) {
            plugin.getCache().addPlayer(player);
        } else {
            try {
                Reader reader = new FileReader(file);
                MCRealisticPlayer mcRealisticPlayer = plugin.getGson().fromJson(reader, MCRealisticPlayer.class);
                plugin.getCache().addPlayer(mcRealisticPlayer);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        MCRealisticPlayer mcRealisticPlayer = plugin.getCache().getPlayer(player);
        if (mcRealisticPlayer != null) {
            try {
                mcRealisticPlayer.save(plugin);
                plugin.getCache().removePlayer(player);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
