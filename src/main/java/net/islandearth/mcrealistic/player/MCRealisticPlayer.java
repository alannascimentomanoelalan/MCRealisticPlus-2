package net.islandearth.mcrealistic.player;

import com.google.gson.Gson;
import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.translation.Translations;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.UUID;

public class MCRealisticPlayer {

    private final UUID player;

    private int thirst;
    private int fatigue;
    private int waypointX;
    private int waypointY;
    private int waypointZ;
    private boolean isWarm;
    private boolean nearWarmthSource;
    private boolean hasBrokenBones;

    public MCRealisticPlayer(Player player) {
        this.player = player.getUniqueId();
        this.isWarm = true;
    }

    public int getFatigue() {
        return fatigue;
    }

    public void setFatigue(int fatigue) {
        this.fatigue = fatigue;
    }

    public int getThirst() {
        return thirst;
    }

    public void setThirst(int thirst) {
        this.thirst = thirst;
    }

    public int getWaypointX() {
        return waypointX;
    }

    public int getWaypointY() {
        return waypointY;
    }

    public int getWaypointZ() {
        return waypointZ;
    }

    public void setWaypointX(int waypointX) {
        this.waypointX = waypointX;
    }

    public void setWaypointY(int waypointY) {
        this.waypointY = waypointY;
    }

    public void setWaypointZ(int waypointZ) {
        this.waypointZ = waypointZ;
    }

    public boolean isWarm() {
        return isWarm;
    }

    public void setWarm(boolean warm) {
        isWarm = warm;
    }

    public boolean isNearWarmthSource() {
        return nearWarmthSource;
    }

    public void setNearWarmthSource(boolean nearWarmthSource) {
        this.nearWarmthSource = nearWarmthSource;
    }

    public boolean hasBrokenBones() {
        return hasBrokenBones;
    }

    public void setHasBrokenBones(boolean hasBrokenBones) {
        this.hasBrokenBones = hasBrokenBones;
    }

    public Player getBukkitPlayer() {
        return Bukkit.getPlayer(player);
    }

    public void giveRespawnItems(MCRealistic plugin) {
        if (plugin.getConfig().getBoolean("Server.Player.Spawn with items")) {
            getBukkitPlayer().getInventory().addItem(new ItemStack(Material.GLASS_BOTTLE));
            getBukkitPlayer().getInventory().addItem(new ItemStack(Material.WOODEN_AXE));
        }

        if (plugin.getConfig().getBoolean("Server.Messages.Respawn")) {
            Translations.RESPAWN.sendList(getBukkitPlayer());
        }
    }

    public void save(MCRealistic plugin) throws IOException {
        File file = new File(plugin.getDataFolder() + "/players/" + this.player + ".json");
        Writer writer = new FileWriter(file);
        Gson gson = plugin.getGson();
        gson.toJson(this, writer);
        writer.flush();
        writer.close();
    }

    public boolean delete(MCRealistic plugin) throws IOException {
        File file = new File(plugin.getDataFolder() + "/regions/" + this.player + ".json");
        return file.delete();
    }
}
