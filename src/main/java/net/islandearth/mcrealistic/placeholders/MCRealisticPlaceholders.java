package net.islandearth.mcrealistic.placeholders;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class MCRealisticPlaceholders extends PlaceholderExpansion {

    private final MCRealistic plugin;

    public MCRealisticPlaceholders(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @Override
    public String onPlaceholderRequest(Player player, String placeholder) {
        if (player == null) return "";
        MCRealisticPlayer mcRealisticPlayer = plugin.getCache().getPlayer(player);
        switch (placeholder.toLowerCase()) {
            case "thirst":
                return String.valueOf(mcRealisticPlayer.getThirst());
            case "fatigue":
                return String.valueOf(mcRealisticPlayer.getFatigue());
            case "infected":
                return String.valueOf(plugin.getDiseases().contains(player.getUniqueId()) || plugin.getColds().contains(player.getUniqueId()));
        }
        return null;
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }

    @Override
    public String getAuthor() {
        return "SamB440";
    }

    @Override
    public String getIdentifier() {
        return "mcrealistic";
    }

    @Override
    public String getVersion() {
        return plugin.getDescription().getVersion();
    }
}
