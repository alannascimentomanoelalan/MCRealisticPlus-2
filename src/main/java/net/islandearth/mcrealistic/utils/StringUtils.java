package net.islandearth.mcrealistic.utils;

import net.md_5.bungee.api.ChatColor;

public class StringUtils {

    public static String colour(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }

}
