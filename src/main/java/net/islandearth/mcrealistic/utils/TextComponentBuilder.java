package net.islandearth.mcrealistic.utils;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;

public class TextComponentBuilder {

    private final TextComponent text;

    public TextComponentBuilder(final String text) {
        this.text = new TextComponent(text);
    }

    public TextComponentBuilder setCommand(final String command) {
        text.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/" + command));
        return this;
    }

    public TextComponentBuilder setHover(final String hover) {
        text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(hover)));
        return this;
    }

    public TextComponentBuilder setLink(final String link) {
        text.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, link));
        return this;
    }

    public TextComponent build() {
        return text;
    }

}
