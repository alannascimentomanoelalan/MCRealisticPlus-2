package net.islandearth.mcrealistic.utils;

import com.google.common.collect.ImmutableList;
import org.bukkit.Material;

import java.util.Arrays;
import java.util.List;

public enum ExtraTags {

    AXES(Arrays.asList(
            Material.WOODEN_AXE,
            Material.STONE_AXE,
            Material.IRON_AXE,
            Material.DIAMOND_AXE,
            Material.GOLDEN_AXE,
            Material.NETHERITE_AXE));

    public ImmutableList<Material> getMaterials() {
        return ImmutableList.copyOf(materials);
    }

    private List<Material> materials;

    ExtraTags(List<Material> materials) {
        this.materials = materials;
    }
}
