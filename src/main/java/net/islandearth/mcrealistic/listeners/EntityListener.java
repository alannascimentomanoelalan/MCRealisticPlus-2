package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class EntityListener implements Listener {

    private final MCRealistic plugin;

    public EntityListener(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerFall(EntityDamageEvent ede) {
        if (ede.getEntity() instanceof Player) {
            Player player = (Player) ede.getEntity();
            MCRealisticPlayer mcRealisticPlayer = plugin.getCache().getPlayer(player);
            if (mcRealisticPlayer == null) return;
            if (!Utils.isWorldEnabled(player.getWorld()) || mcRealisticPlayer.hasBrokenBones()) return;

            if (plugin.getConfig().getBoolean("Server.Player.Broken_Bones.Enabled")) {
                if (player.getGameMode() != GameMode.CREATIVE && player.getGameMode() != GameMode.SPECTATOR) {
                    if (ede.getCause() == EntityDamageEvent.DamageCause.FALL
                            && player.getFallDistance() >= 7.0f
                            && !ede.isCancelled()) {

                        Translations.BROKEN_BONES.send(player);
                        mcRealisticPlayer.setHasBrokenBones(true);

                        long boneRepairTime = (long) (player.getFallDistance() * 80.0f);
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (int) boneRepairTime, 1));
                        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                            if (mcRealisticPlayer.hasBrokenBones()) {
                                Translations.LEGS_HEALED.send(player);
                                mcRealisticPlayer.setHasBrokenBones(false);
                            }
                        }, boneRepairTime);
                    }
                }
            }
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
