package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.util.List;

public class RespawnListener implements Listener {

    private final MCRealistic plugin;
    private final List<World> worlds;

    public RespawnListener(MCRealistic plugin) {
        this.plugin = plugin;
        this.worlds = plugin.getWorlds();
    }

    @EventHandler
    public void onPlayerSpawn(PlayerRespawnEvent pre) {
        Player player = pre.getPlayer();
        MCRealisticPlayer mcRealisticPlayer = plugin.getCache().getPlayer(player);
        if (mcRealisticPlayer == null) return;
        if (worlds.contains(player.getWorld())) {
            mcRealisticPlayer.setThirst(0);
            mcRealisticPlayer.setFatigue(0);
            mcRealisticPlayer.giveRespawnItems(plugin);
            plugin.getColds().remove(player.getUniqueId());
            plugin.getDiseases().remove(player.getUniqueId());
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
