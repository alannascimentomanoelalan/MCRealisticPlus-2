package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

public class InventoryListener implements Listener {

    private final MCRealistic plugin;
    private final List<World> worlds;

    public InventoryListener(MCRealistic plugin) {
        this.plugin = plugin;
        this.worlds = plugin.getWorlds();
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            Bukkit.getOnlinePlayers().forEach(player -> {
                if (getConfig().getBoolean("Server.Player.Weight")
                        && worlds.contains(player.getWorld())
                        && player.getGameMode() != GameMode.CREATIVE
                        && player.getGameMode() != GameMode.SPECTATOR) {
                    int totalWeight = 0;
                    for (ItemStack armorContent : player.getInventory().getArmorContents()) {
                        if (armorContent != null && armorContent.getType() != Material.AIR) {
                            int weight = getWeight(armorContent);
                            if (weight != 0) {
                                totalWeight = totalWeight + (weight - 1);
                            }
                        }
                    }

                    if (totalWeight == 0) {
                        return;
                    }

                    player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 25, totalWeight / 2));
                }
            });
        }, 0L, 20L);
    }

    private int getWeight(ItemStack itemStack) {
        switch (itemStack.getType()) {
            case CHAINMAIL_BOOTS:
            case CHAINMAIL_LEGGINGS:
            case CHAINMAIL_HELMET:
            case CHAINMAIL_CHESTPLATE:
                return getConfig().getInt("Server.Player.Weight.Chainmail");
            case GOLDEN_BOOTS:
            case GOLDEN_LEGGINGS:
            case GOLDEN_HELMET:
            case GOLDEN_CHESTPLATE:
                return getConfig().getInt("Server.Player.Weight.Gold");
            case LEATHER_BOOTS:
            case LEATHER_LEGGINGS:
            case LEATHER_HELMET:
            case LEATHER_CHESTPLATE:
                return getConfig().getInt("Server.Player.Weight.Leather");
            case IRON_BOOTS:
            case IRON_LEGGINGS:
            case IRON_HELMET:
            case IRON_CHESTPLATE:
                return getConfig().getInt("Server.Player.Weight.Iron");
            case DIAMOND_BOOTS:
            case DIAMOND_LEGGINGS:
            case DIAMOND_HELMET:
            case DIAMOND_CHESTPLATE:
                return getConfig().getInt("Server.Player.Weight.Diamond");
            case NETHERITE_BOOTS:
            case NETHERITE_LEGGINGS:
            case NETHERITE_HELMET:
            case NETHERITE_CHESTPLATE:
                return getConfig().getInt("Server.Player.Weight.Netherite");
            default:
                return 0;
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
