package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.TitleManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class ConsumeListener implements Listener {

    private final MCRealistic plugin;
    private final List<World> worlds;

    public ConsumeListener(MCRealistic plugin) {
        this.plugin = plugin;
        this.worlds = plugin.getWorlds();
    }

    @EventHandler
    public void onItemConsume(PlayerItemConsumeEvent pice) {
        Player player = pice.getPlayer();
        MCRealisticPlayer mcRealisticPlayer = plugin.getCache().getPlayer(player);
        if (mcRealisticPlayer == null) return;
        ItemStack item = pice.getItem();
        if (worlds.contains(player.getWorld())) {
            if (player.getGameMode() == GameMode.SURVIVAL) {
                if (item.hasItemMeta() && item.getType().equals(Material.POTION)) {
                    PotionMeta pm = (PotionMeta) item.getItemMeta();
                    if (pm != null && pm.getBasePotionData().getType() == PotionType.WATER) {
                        if (getConfig().getBoolean("Server.Player.Thirst.Enabled")) {
                            if (getConfig().getBoolean("Server.Player.Thirst.Require_Purify")) {
                                if (item.getItemMeta().hasLore() && item.getItemMeta().getLore() != null) {
                                    if (item.getItemMeta().getLore().get(0).equals(ChatColor.GRAY + "Purified")) {
                                        Translations.NOT_THIRSTY.send(player);
                                        int fatigueReduction = mcRealisticPlayer.getFatigue() == 0 ? 0 : mcRealisticPlayer.getFatigue() - 2;
                                        mcRealisticPlayer.setFatigue(fatigueReduction);
                                        mcRealisticPlayer.setThirst(0);
                                    }
                                } else {
                                    player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 120, 2));
                                    player.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 180, 1));
                                }
                            } else {
                                Translations.NOT_THIRSTY.send(player);
                                int fatigueReduction = mcRealisticPlayer.getFatigue() == 0 ? 0 : mcRealisticPlayer.getFatigue() - 2;
                                mcRealisticPlayer.setFatigue(fatigueReduction);
                                mcRealisticPlayer.setThirst(0);
                            }
                        }
                    } else {
                        if (item.hasItemMeta() && item.getItemMeta() != null
                            && item.getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Medicine")) {
                            if (item.getItemMeta().getLore() != null
                                    && item.getItemMeta().getLore().equals(Arrays.asList(ChatColor.WHITE + "Drink to help fight your cold/disease!"))) {

                                List<UUID> hasDisease = getDiseases();
                                List<UUID> hasCold = getColds();

                                if (hasCold.contains(player.getUniqueId())) {
                                    TitleManager.sendTitle(player, "", Translations.SUBSIDE.get(player, "cold"), 200);
                                    hasCold.remove(player.getUniqueId());
                                } else if (hasDisease.contains(player.getUniqueId())) {
                                    hasDisease.remove(player.getUniqueId());
                                    TitleManager.sendTitle(player, "", Translations.SUBSIDE.get(player, "disease"), 200);
                                }
                            }
                        }
                    }
                } else {
                    if (item.getType() == Material.MILK_BUCKET && mcRealisticPlayer.hasBrokenBones()) {
                        PotionEffect effect = player.getPotionEffect(PotionEffectType.SLOW);
                        if (effect != null) {
                            Bukkit.getScheduler().runTaskLater(plugin, () -> player.addPotionEffect(effect), 1L);
                        }
                    }

                    if (isRaw(item.getType())
                        && getConfig().getBoolean("Server.Player.Raw_Food_Illness")) {
                        Random random = ThreadLocalRandom.current();
                        int randomPoison = random.nextInt(2);
                        if (randomPoison == 1) {
                            player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 60, 1));
                            player.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 120, 0));
                        }
                    }
                }
            }
        }
    }

    private boolean isRaw(Material material) {
        return material.toString().contains("RAW");
    }

    private List<UUID> getDiseases() {
        return plugin.getDiseases();
    }

    private List<UUID> getColds() {
        return plugin.getColds();
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
