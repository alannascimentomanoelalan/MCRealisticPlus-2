/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ConnectionListener implements Listener {

    private final MCRealistic plugin;

    public ConnectionListener(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent pje) {
        Bukkit.getScheduler().runTask(plugin, () -> {
            Player player = pje.getPlayer();
            MCRealisticPlayer mcRealisticPlayer = plugin.getCache().getPlayer(player);
            if (!player.hasPlayedBefore()) {
                mcRealisticPlayer.giveRespawnItems(plugin);
            }

            if (mcRealisticPlayer.getWaypointX() != 0 && mcRealisticPlayer.getWaypointZ() != 0) {
                player.setCompassTarget(new Location(player.getWorld(), mcRealisticPlayer.getWaypointX(), mcRealisticPlayer.getWaypointY(), mcRealisticPlayer.getWaypointZ()));
            }

            if (mcRealisticPlayer.hasBrokenBones()) {
                PotionEffect potionEffect = player.getPotionEffect(PotionEffectType.SLOW);
                if (potionEffect != null) {
                    long boneRepairTime = potionEffect.getDuration();
                    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                        if (mcRealisticPlayer.hasBrokenBones()) {
                            Translations.LEGS_HEALED.send(player);
                            mcRealisticPlayer.setHasBrokenBones(false);
                        }
                    }, boneRepairTime);
                } else {
                    mcRealisticPlayer.setHasBrokenBones(false);
                }
            }
        });
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
