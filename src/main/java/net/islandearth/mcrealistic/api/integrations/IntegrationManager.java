package net.islandearth.mcrealistic.api.integrations;

import net.islandearth.mcrealistic.MCRealistic;
import org.bukkit.Location;

public abstract class IntegrationManager {

    private final MCRealistic plugin;

    public IntegrationManager(MCRealistic plugin) {
        this.plugin = plugin;
    }

    public MCRealistic getPlugin() {
        return plugin;
    }

    /**
     * Checks if the specified location is within a region.
     *
     * @param location location to check
     * @return true if location is within a region, false otherwise
     */
    public abstract boolean isInRegion(Location location);

}
