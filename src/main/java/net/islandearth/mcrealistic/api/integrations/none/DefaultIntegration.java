package net.islandearth.mcrealistic.api.integrations.none;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.api.integrations.IntegrationManager;
import org.bukkit.Location;

/**
 * Used if the server has no protection integration.
 */
public class DefaultIntegration extends IntegrationManager {

    public DefaultIntegration(MCRealistic plugin) {
        super(plugin);
    }

    @Override
    public boolean isInRegion(Location location) {
        return false;
    }
}
