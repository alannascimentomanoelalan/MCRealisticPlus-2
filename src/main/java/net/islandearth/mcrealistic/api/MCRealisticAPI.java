package net.islandearth.mcrealistic.api;

import net.islandearth.languagy.api.language.Translator;

public interface MCRealisticAPI {
    /**
     * Gets the translator provided by Languagy
     *
     * @return Translator
     */
    Translator getTranslator();
}
