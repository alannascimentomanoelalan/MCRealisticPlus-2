package net.islandearth.mcrealistic.translation;

import me.clip.placeholderapi.PlaceholderAPI;
import net.islandearth.languagy.api.language.Language;
import net.islandearth.mcrealistic.MCRealistic;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum Translations {
    NO_PERMISSION("&cYou do not have the required permission {0}"),
    NOT_TIRED("&aI don't feel tired anymore..."),
    TOO_TIRED("&cI'm too tired to do that."),
    TIRED("&f&cI am tired..."),
    VERY_TIRED("&cI am very tired... I should get some sleep."),
    NO_HAND_CHOP("&cYou can't chop down trees with your hands!"),
    NOT_THIRSTY("&aI'm not thirsty anymore!"),
    LITTLE_THIRSTY("I am a little thirsty..."),
    GETTING_THIRSTY("&cI am getting thirsty..."),
    REALLY_THIRSTY("&c&l&nI am really thirsty... I should drink some water."),
    COSY("&2I feel cosy..."),
    COLD("&c&nI am cold, I should wear some clothes (Armour)."),
    HURT("&c&lI am hurt!"),
    HUNGRY("&c&lI am hungry! I should really eat something..."),
    SHOULD_SLEEP("&cI should sleep in that bed..."),
    USED_BANDAGE("&aYou used a bandage, and your legs healed!"),
    LEGS_HEALED("&aYour legs healed!"),
    BROKEN_BONES("&cYou fell from a high place and broke your bones!"),
    WAYPOINT_SET("&aA waypoint has been set at your current location!"),
    ITEMS("MCRealistic Items"),
    RESPAWN("&6Type /fatigue to see your fatigue!", "&6Type /thirst to see your thirst!"),
    CAUGHT_COLD("&cYou have caught a cold!"),
    MEDICINE_TIP("&a&bTIP: &fUse medicine to fight the %0!"),
    COLD_DEVELOPED("&cYour cold developed into a disease!"),
    DISEASE_DAMAGE("&cThe disease begins to damage your body..."),
    SUBSIDE("&aThe %0 beings to subside...");

    private final List<String> defaultValue;
    private final boolean isList;

    Translations(String defaultValue) {
        this.defaultValue = Collections.singletonList(defaultValue);
        this.isList = false;
    }

    Translations(String defaultValue, boolean isList) {
        this.defaultValue = Arrays.asList(defaultValue);
        this.isList = isList;
    }

    Translations(String... defaultValues) {
        this.defaultValue = Arrays.asList(defaultValues);
        this.isList = true;
    }

    public String getDefaultValue() {
        return defaultValue.get(0);
    }

    public List<String> getDefaultValues() {
        return defaultValue;
    }

    public boolean isList() {
        return isList;
    }

    private String getPath() {
        return this.toString().toLowerCase();
    }

    public void send(Player player) {
        String message = MCRealistic.getAPI().getTranslator().getTranslationFor(player, this.getPath());
        send(player, setPapi(player, message));
    }

    public void send(Player player, String... values) {
        String message = MCRealistic.getAPI().getTranslator().getTranslationFor(player, this.getPath());
        message = this.setPapi(player, replaceVariables(message, values));
        send(player, message);
    }

    public void sendList(Player player) {
        List<String> messages = MCRealistic.getAPI().getTranslator().getTranslationListFor(player, this.getPath());
        messages.forEach(message -> player.sendMessage(this.setPapi(player, message)));
    }

    public void sendList(Player player, String... values) {
        List<String> messages = MCRealistic.getAPI().getTranslator().getTranslationListFor(player, this.getPath());
        messages.forEach(message -> {
            message = this.setPapi(player, replaceVariables(message, values));
            send(player, message);
        });
    }

    public String get(Player player) {
        return this.setPapi(player, MCRealistic.getAPI().getTranslator().getTranslationFor(player, this.getPath()));
    }

    public String get(Player player, String... values) {
        String message = MCRealistic.getAPI().getTranslator().getTranslationFor(player, this.getPath());
        message = replaceVariables(message, values);
        return this.setPapi(player, message);
    }

    public List<String> getList(Player player) {
        List<String> list = new ArrayList<>();
        MCRealistic.getAPI().getTranslator().getTranslationListFor(player, this.getPath()).forEach(text -> list.add(this.setPapi(player, text)));
        return list;
    }

    public List<String> getList(Player player, String... values) {
        List<String> messages = new ArrayList<>();
        MCRealistic.getAPI().getTranslator()
                .getTranslationListFor(player, this.getPath())
                .forEach(message -> messages.add(this.setPapi(player, replaceVariables(message, values))));
        return messages;
    }

    public static void generateLang(MCRealistic plugin) {
        File lang = new File(plugin.getDataFolder() + "/lang/");
        lang.mkdirs();

        for (Language language : Language.values()) {
            try {
                plugin.saveResource("lang/" + language.getCode() + ".yml", false);
                plugin.getLogger().info("Generated " + language.getCode() + ".yml");
            } catch (IllegalArgumentException ignored) {
            }

            File file = new File(plugin.getDataFolder() + "/lang/" + language.getCode() + ".yml");
            if (file.exists()) {
                FileConfiguration config = YamlConfiguration.loadConfiguration(file);
                for (Translations key : values()) {
                    if (config.get(key.toString().toLowerCase()) == null) {
                        plugin.getLogger().warning("No value in translation file for key "
                                + key.toString() + " was found. Regenerate language files?");
                    }
                }
            }
        }
    }

    @NotNull
    private String replaceVariables(String message, String... values) {
        String modifiedMessage = message;
        for (int i = 0; i < 10; i++) {
            if (values.length > i) modifiedMessage = modifiedMessage.replaceAll("%" + i, values[i]);
            else break;
        }
        return modifiedMessage;
    }

    @NotNull
    private String setPapi(Player player, String message) {
        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            return PlaceholderAPI.setPlaceholders(player, message);
        }
        return message;
    }

    protected void send(Player player, String message) {
        player.spigot().sendMessage(getMessageType(), new TextComponent(message));
    }

    public ChatMessageType getMessageType() {
        String configSetting = JavaPlugin.getPlugin(MCRealistic.class).getConfig().getString("Server.Messages.Type");
        if (configSetting == null) return ChatMessageType.CHAT;
        if (configSetting.equals("MESSAGE") || configSetting.equals("CHAT")) {
            return ChatMessageType.CHAT;
        }
        return ChatMessageType.ACTION_BAR;
    }
}
